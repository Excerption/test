#include "StudentGradeManager.h"

void StudentGradeManager::GradeScores(std::string inputPath, std::string outputPath)
{
	std::list<Student>students = _reader->ReadStudents(inputPath);
	std::list<Student>gradedStudents = _grader.GradeScores(students);
	_writer->WriteStudents(gradedStudents, outputPath);
}
