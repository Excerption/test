#pragma once

#include "IStudentWriter.h"
#include "ITextWriterFactory.h"
#include "TextWriterFactory.h"

class StudentCSVWriter : IStudentWriter
{
public:
	void IStudentWriter::WriteStudents(const std::list<Student>& students, const std::string& outputPath);
	StudentCSVWriter()
	{
		_writer = reinterpret_cast<ITextWriterFactory*>(new TextWriterFactory);
	}
	StudentCSVWriter(ITextWriterFactory* writer)
	{
		_writer = writer;
	}
	~StudentCSVWriter()
	{
		delete _writer;
	}
private:
	ITextWriterFactory* _writer;
};