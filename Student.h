#pragma once

#include <string>

class Student
{
public:
	void SetFirstName(std::string FistName);
	void SetLastName(std::string LastName);
	void SetScore(int Score);

	std::string GetFirstName() const;
	std::string GetLastName() const;
	int GetScore() const;

private:
	std::string _FirstName;
	std::string _LastName;
	int _Score;
};
