#pragma once

#include <string>

class PathManager
{
public:
	static std::string CreateOutputPathFromInputPathWithPostfix(std::string& inputPath, const std::string& postfix);
};