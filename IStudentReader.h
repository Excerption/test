#pragma once

#include "Student.h"

#include <list>


class IStudentReader
{
public:
	virtual std::list<Student> ReadStudents(const std::string& inputPath) = 0;
};