#include "PathManager.h"

#include <experimental/filesystem>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>

namespace fs = std::experimental::filesystem;

std::string getFileNameWithoutExtension(const std::string& inputPath)
{

	char sep = '\\';

	size_t i = inputPath.rfind(sep, inputPath.length());
	if (i != std::string::npos) 
		return(inputPath.substr(i + 1, inputPath.length() - i - 5));
	
	return("");
}

std::string getDirectoryName(const std::string& inputPath)
{

	char sep = '\\';

	size_t i = inputPath.rfind(sep, inputPath.length());
	if (i != std::string::npos)
		return(inputPath.substr(0, i));

	return("");
}

std::string PathManager::CreateOutputPathFromInputPathWithPostfix(std::string & inputPath, const std::string & postfix)
{
	std::string directoryPath = getDirectoryName(inputPath);
	//std::string appDirectiryPath;
	std::string inputFileName = getFileNameWithoutExtension(inputPath);

	size_t coloum = inputPath.find_first_of(':');
	if (coloum == std::string::npos)
	{
		DWORD  retval = 0;
		TCHAR  buffer[4096] = TEXT("");
		TCHAR  buf[4096] = TEXT("");
		TCHAR** lppPart = { NULL };

		retval = GetFullPathName(inputPath.c_str(), 4096, buffer, lppPart);

		if (retval == 0)
		{
			throw std::invalid_argument("GetFullPathName failed.");
		}

		inputPath = buffer;
		inputFileName = getFileNameWithoutExtension(inputPath);
		directoryPath = getDirectoryName(inputPath);
	}
	
	if (!fs::exists(inputPath))
	{
		throw std::invalid_argument("File doesn't exists");
	}

	if (postfix.empty())
		throw std::invalid_argument("ArgumentNullException(postfix)");
		
	//std::string inputFileName = getFileNameWithoutExtension(inputPath);
	// It is possible situation where there will be the invalid file name characters in the postfix string.
	// We could check it by using Path.GetInvalidFileNameChars method and check each character, but in our case
	// when we have fixed prefix and for code simplicity we omit this check.
	std::string outputFileName = inputFileName + "-" + postfix + ".txt";

	
	std::string outputPath = directoryPath + "\\" + outputFileName;

	return outputPath;
}
