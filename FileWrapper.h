#pragma once
#include "IFile.h"

class FileWrapper : IFile 
{
public:
	bool IFile::Exists(const std::string& path);
};