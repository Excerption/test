#include "TextReaderFactory.h"

#include <fstream>

std::ifstream TextReaderFactory::CreateTextReaderFromFilePath(const std::string& path)
{
	std::ifstream reader(path);
	return reader;
}