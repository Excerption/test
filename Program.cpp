#include "Student.h"
#include "StudentGrader.h"
#include "PathManager.h"
#include "StudentCSVReader.h"
#include "StudentCSVWriter.h"
#include "StudentGradeManager.h"

#include <iostream>
#include <exception>

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "There are should be provided an argument to the grade-scores.exe with the path to the file with students data.";
		return 1;
	}

	try
	{
		//std::string appPath = argv[0];
		std::string inputPath = argv[1];

		//std::string outputPath = PathManager::CreateOutputPathFromInputPathWithPostfix(appPath, inputPath, "graded");

		//IStudentReader* reader;
		PathManager pathManager;
		std::string postfix = "graded";
		std::string outputPath = pathManager.CreateOutputPathFromInputPathWithPostfix(inputPath, postfix);
		IStudentReader* reader = reinterpret_cast<IStudentReader*>(new StudentSCVreader);
		IStudentWriter* writer = reinterpret_cast<IStudentWriter*>(new StudentCSVWriter);
		StudentGradeManager manager(reader, writer);
		//std::list<Student> studenst = StudentSCVreader::ReadStudents(inputPath);
		std::list<Student> studenst = reader->ReadStudents(inputPath);
		std::list<Student> studenst_sorted = StudentGrader::GradeScores(studenst);
		//StudentCSVWriter::WriteStudents(studenst_sorted, outputPath);
	}

	catch (...)
	{
		return 1;
	}
	return 0;
}