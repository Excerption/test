#pragma once

#include "Student.h"

#include <list>

class StudentGrader
{
	public:
		static std::list<Student>& GradeScores(std::list<Student>& students);
};

