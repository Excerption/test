#include "StudentCSVWriter.h"

#include <iostream>
#include <fstream>

void StudentCSVWriter::WriteStudents(const std::list<Student>& students, const std::string& outputPath)
{
	std::ofstream writer = this->_writer->CreateTextWriterFromFilePath(outputPath);
	//writer.open(outputPath);
	std::list<Student>::const_iterator it;
	for (it = students.begin(); it != students.end(); ++it)
	{
		writer << it->GetFirstName() << ", " << it->GetLastName() << ", " << it->GetScore() << "\n";
	}
	writer.close();
}
