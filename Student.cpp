#include "Student.h"

void Student::SetFirstName(std::string FistName)
{
	_FirstName = FistName;
}
void Student::SetLastName(std::string LastName)
{
	_LastName = LastName;
}
void Student::SetScore(int Score)
{
	_Score = Score;
}

std::string Student::GetFirstName() const
{
	return _FirstName;
}
std::string Student::GetLastName() const
{
	return _LastName;
}
int Student::GetScore() const
{
	return _Score;
}