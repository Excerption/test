#pragma once

#include "ITextWriterFactory.h"

class TextWriterFactory : ITextWriterFactory
{
public:
	std::ofstream ITextWriterFactory::CreateTextWriterFromFilePath(const std::string& path);
};
