#pragma once

#include<string>

class ITextReaderFactory
{
public:
	virtual std::ifstream CreateTextReaderFromFilePath(const std::string& path) = 0;
};