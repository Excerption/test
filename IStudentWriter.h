#pragma once

#include "Student.h"

#include <list>

class IStudentWriter
{
public:
	virtual void WriteStudents(const std::list<Student>& students, const std::string& outputPath) = 0;
};

