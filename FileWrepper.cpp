#include "FileWrapper.h"

#include <fstream>

//#include <experimental/filesystem>

//namespace fs = std::experimental::filesystem;

bool FileWrapper::Exists(const std::string& path)
{
	std::ifstream f(path.c_str());
	return f.good();
	//std::filesystem::exists()
	//return fs::exists(path);
}