#pragma once

#include "ITextReaderFactory.h"

class TextReaderFactory : ITextReaderFactory
{
public:
	std::ifstream ITextReaderFactory::CreateTextReaderFromFilePath(const std::string& path);
};