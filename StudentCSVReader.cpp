#include "StudentCSVReader.h"

#include <sstream>
//#include <string>
//#include <iostream>
#include <fstream>
#include <cstdint>
#include<vector>
//#include <experimental/filesystem>

//namespace fs = std::experimental::filesystem;

std::list<Student> StudentSCVreader::ReadStudents(const std::string& inputPath)
{
	if (this->_fileWrapper->Exists(inputPath))
	{
		throw std::invalid_argument("File doesn't exists");
	}

	std::ifstream  reader = this->_reader->CreateTextReaderFromFilePath(inputPath);
	std::list<Student> students;

	//std::ifstream  data(inputPath);
	std::string line;

	while (std::getline(reader, line))
	{
		std::stringstream  lineStream(line);
		std::string        cell;
		std::vector<std::string> colums;

		while (std::getline(lineStream, cell, ','))
		{
			if (cell.empty())
			{
				throw std::invalid_argument("Empty colum.");	
			}

			cell.erase(cell.find_last_not_of(" \n\r\t") + 1);
			cell.erase(cell.begin(), cell.begin() + cell.find_first_not_of(" \n\r\t"));

			colums.push_back(cell);
		}

		if (colums.size() < 3)
		{
			throw std::invalid_argument("Not enough colums.");
		}
		
		int score = 0;
		score = std::stoi(colums.at(2)); 
		Student student;
		student.SetFirstName(colums.at(0));
		student.SetLastName(colums.at(1));
		student.SetScore(score);

		students.push_back(student);
	}
	return students;
}
