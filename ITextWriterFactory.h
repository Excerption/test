#pragma once

#include <string>

class ITextWriterFactory
{
public:
	virtual std::ofstream CreateTextWriterFromFilePath(const std::string& path) = 0;
};