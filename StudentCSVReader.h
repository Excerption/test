#pragma once

#include "IStudentReader.h"
#include "ITextReaderFactory.h"
#include "TextReaderFactory.h"
#include "IFile.h"
#include "FileWrapper.h"

class StudentSCVreader : IStudentReader
{
public:
	std::list<Student> IStudentReader::ReadStudents(const std::string& inputPath);
	StudentSCVreader(ITextReaderFactory* reader, IFile* fileWrapper)
	{
		_reader = reader;
		_fileWrapper = fileWrapper;
	}
	StudentSCVreader()
	{
		_reader = reinterpret_cast<ITextReaderFactory*>(new TextReaderFactory);
		_fileWrapper = reinterpret_cast<IFile*>(new FileWrapper);
	}
	~StudentSCVreader()
	{
		delete _reader;
		delete _fileWrapper;
	}

private:
	ITextReaderFactory* _reader;
	IFile* _fileWrapper;
};