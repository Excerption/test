#pragma once

#include "StudentGrader.h"
#include "IStudentReader.h"
#include "IStudentWriter.h"

class StudentGradeManager
{
public:
	StudentGradeManager(IStudentReader* reader, IStudentWriter* writer)
	{
		_reader = reader;
		_writer = writer;
	}

	//void GradeScores(std::string inputPath, std::string outputPath)
	//{
		//std::list<Student> students = _reader->ReadStudents(inputPath);
	//}

	void GradeScores(std::string inputPath, std::string outputPath);
	//{
	//	var students = _reader.ReadStudents(inputPath);
	//	var gradedStudents = _grader.GradeScores(students);
	//	_writer.WriteStudents(gradedStudents, outputPath);
	//}
	
private:
	 StudentGrader _grader;
	 IStudentReader* _reader;
	 IStudentWriter* _writer;
};