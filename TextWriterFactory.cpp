#include "TextWriterFactory.h"

#include <fstream>

std::ofstream TextWriterFactory::CreateTextWriterFromFilePath(const std::string& path)
{
	std::ofstream writer;
	writer.open(path);
	return writer;
}