#include "FileWrapper.h"

#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;


bool FileWrapper::Exists(const std::string& path)
{
	return fs::exists(path);
}