#include "StudentGrader.h"

#include <algorithm>

bool caseInsensitiveStringCompare(const std::string& str1, const std::string& str2)
{
	std::string lover1(str1);
	std::string lover2(str2);
	
	std::transform(lover1.begin(), lover1.end(), lover1.begin(), ::tolower);
	std::transform(lover2.begin(), lover2.end(), lover2.begin(), ::tolower);

	if (strcmp(lover1.c_str(), lover2.c_str()) < 0)
		return true;
	return false;
}

std::list<Student>& StudentGrader::GradeScores(std::list<Student>& students)
{
	if (students.empty())
		return students;

	struct sort_students_by_score_struct 
	{
		bool operator()(const Student& x, const Student& y)
		{
			if (x.GetScore() > y.GetScore())
				return true;
			else if (x.GetScore() < y.GetScore())
				return false;
			else if (caseInsensitiveStringCompare(x.GetLastName(), y.GetLastName()))
				return true;
			else if (!(caseInsensitiveStringCompare(x.GetLastName(), y.GetLastName())))
				return false;
			else if (caseInsensitiveStringCompare(x.GetFirstName(), y.GetFirstName()))
				return true;
			else return false;
		}
	} sort_students_by_score;

	students.sort(sort_students_by_score);

	return students;
}
