#pragma once

#include<string>

class IFile
{
public:
	virtual bool Exists(const std::string& path) = 0;
};